CREATE TABLE [notification] (
	[id] Integer PRIMARY KEY,
	[title] nvarchar(50),
	[message] nvarchar(50),
	[status] default 0,
	[time] datetime NULL,
	[flag] Integer

);

CREATE TABLE [archive] (
	[id] Integer PRIMARY KEY,
	[title] nvarchar(50),
	[message] nvarchar(50),
	[time] datetime NULL
);
