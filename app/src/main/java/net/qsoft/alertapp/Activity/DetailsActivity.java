package net.qsoft.alertapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import net.qsoft.alertapp.App.AppController;
import net.qsoft.alertapp.Database.DAO;
import net.qsoft.alertapp.Database.DatabaseHelper;
import net.qsoft.alertapp.Model.Message;
import net.qsoft.alertapp.R;

import java.util.ArrayList;

import static net.qsoft.alertapp.Database.DBHelper.TABLE_NOTIFICATION;

public class DetailsActivity extends AppCompatActivity {
    TextView user_id, message_title, message_body;
    String id, title, message;
    int status;
    Button deleteBtn;
    public ArrayList<Message> messagesList;
    DatabaseHelper databaseHelper;
    String local_data_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestFullScreenWindow();
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        //deleteBtn = (Button) findViewById(R.id.btn_delete);
        databaseHelper = new DatabaseHelper(getApplicationContext());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = null;
                title = null;
                message = null;
                status= Integer.parseInt(null);
                getSupportActionBar().setTitle("Message");

            } else {
                id = extras.getString("id");
                title = extras.getString("title");
                message = extras.getString("message");
                status = extras.getInt("status");
                status=1;

                setView(title, message);
                getSupportActionBar().setTitle(title);

             // DAO.executeSQL("UPDATE" +TABLE_NOTIFICATION+ "SET status = \"1\" WHERE id =" +id);

                DAO dao=new DAO(AppController.getInstance());
                dao.open();
                dao.UpdateStatus(status,id);
                dao.close();

//                // Link to log in Screen
//                deleteBtn.setOnClickListener(new View.OnClickListener() {
//
//                    public void onClick(View view) {
//                        databaseHelper.deleteSingleMessage(id);
//                        Toast.makeText(getApplicationContext(), "Deleted", Toast.LENGTH_LONG).show();
//                    }
//                });
            }
        } else {
          //  id = (String) savedInstanceState.getSerializable("id");
            title = (String) savedInstanceState.getSerializable("title");
            message = (String) savedInstanceState.getSerializable("message");
        }
    }

    private void setView(String title, String message) {

        user_id = (TextView) findViewById(R.id.id);
        message_title = (TextView) findViewById(R.id.title);
        message_body = (TextView) findViewById(R.id.message);

       // user_id.setText(id);
        message_title.setText(title);
        message_body.setText(message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_notifications, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int option_id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (option_id == R.id.btn_delete) {


            DAO dao=new DAO(AppController.getInstance());
            dao.open();
            dao.deleteSingleMessage(id);
            dao.close();

            //databaseHelper.deleteSingleMessage(id);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(this,MessageActivity.class);
         intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("key",1);
          startActivity(intent);
    }
}
