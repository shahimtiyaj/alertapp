package net.qsoft.alertapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import net.qsoft.alertapp.R;

public class SplashActivity extends AppCompatActivity {
    private Button btnLinkToHome;
    private Button btnLinkToReg;
    private Button btnLinkTologin;
    public static int flag = 0;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestFullScreenWindow();
        setContentView(R.layout.activity_splash);
        toolbarSetup();
        btnLinkToHome = (Button) findViewById(R.id.sign_up_later);
        btnLinkToReg = (Button) findViewById(R.id.reg_button);
        btnLinkTologin = (Button) findViewById(R.id.reg_login);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("key_flag", flag);
        editor.commit();

        // Link to registration Screen
        btnLinkTologin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                sharedpreferences = getApplicationContext().getSharedPreferences(
                        MyPREFERENCES, 0);
                flag = sharedpreferences.getInt("key_flag", flag);
                if (flag == 0) {
                    Intent i = new Intent(getApplicationContext(),
                            MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(getApplicationContext(),
                            MessageActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        });

        // Link to log in Screen
        btnLinkToReg.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        ArchiveMessage.class);
                startActivity(i);
                finish();
            }
        });

        // Link to home Screen
        btnLinkToHome.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MessageActivity.class);
                startActivity(i);
                finish();
            }
        });
    }


    private void toolbarSetup() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

    }

    private void requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
