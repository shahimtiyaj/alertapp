package net.qsoft.alertapp.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import net.qsoft.alertapp.Adapter.MessageAdapter;
import net.qsoft.alertapp.App.AppController;
import net.qsoft.alertapp.Database.DatabaseHelper;
import net.qsoft.alertapp.Model.Message;
import net.qsoft.alertapp.NetworkService.Utils;
import net.qsoft.alertapp.NetworkService.VolleyCustomRequest;
import net.qsoft.alertapp.R;
import net.qsoft.alertapp.Service.NotificationSchedulerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.util.Log.d;
import static android.util.Log.v;
import static net.qsoft.alertapp.Activity.SplashActivity.flag;

public class MainActivity extends AppCompatActivity {
    static final String TAG = MainActivity.class.getSimpleName();
    EditText inputName;
    EditText inputPhone;
    Button btnRegister;
    ProgressDialog pDialog;
    // String name, phone;
    // public static int flag = 0;

    DatabaseHelper databaseHelper;
    private MessageAdapter adapter;
    String db_id;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    public ArrayList<Message> messagesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestFullScreenWindow();
        setContentView(R.layout.registration);
        ViewInitializations();

        //sharedpreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, 0);
        //flag = sharedpreferences.getInt("key_flag", flag);
       // flag = 1;
        //toolbarSetup();


        startService(new Intent(MainActivity.this, NotificationSchedulerService.class));
        Utils.log("Start Service: " + "NotificationSchedulerService");
    }

    public void ViewInitializations() {
        databaseHelper = new DatabaseHelper(getApplicationContext());
        messagesList = new ArrayList<Message>();
        inputName = (EditText) findViewById(R.id.name);
        inputPhone = (EditText) findViewById(R.id.phone);

        btnRegister = (Button) findViewById(R.id.reg_button);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MessageActivity.class);
                startActivity(intent);
                final String name1 = inputName.getText().toString().trim();
                final String phone1 = inputPhone.getText().toString().trim();
                if (inputName.getText().toString().length() == 0) {
                    inputName.setError("Please enter your name");
                    inputName.requestFocus();
                } else if (inputPhone.getText().toString().length() == 0) {
                    inputPhone.setError("Please enter your phone no.");
                    inputPhone.requestFocus();
                } else
                    RegDataSendToServer(name1, phone1);
            }

        });
    }

    public void RegDataSendToServer(String name, String phone) {

        String hitURL = "https://bytelab.000webhostapp.com/Product/reg.php?";

        HashMap<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("phone", phone);

        pDialog.setMessage("Registration Processing ...");
        showDialog();

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d(TAG, "Server Response: " + response.toString());
                        try {
                            v("Response:%n %s", response.toString(4));
                            String status = response.getString("success");
                            String message = response.getString("message");

                            Log.d("Message", status);

                            if (status.equals("1")) {
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Registration not completed!", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        d(TAG, "Error: " + volleyError.getMessage());

                        if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof ServerError) {
                            Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();


                        } else if (volleyError instanceof ParseError) {
                            Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();

                        }
                    }
                });

        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(postRequest);
    }

    private void toolbarSetup() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

    }

    private void requestFullScreenWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
