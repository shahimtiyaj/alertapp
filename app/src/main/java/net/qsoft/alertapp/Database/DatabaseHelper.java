package net.qsoft.alertapp.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import net.qsoft.alertapp.Model.Message;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String LOG = "DatabaseHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "NotificationDatabase";
    private static final String TABLE_NOTIFICATION = "notification";

    // TBL_BRANCH SINGLE field names----------------------------------
    private final static String FLD_MESSAGE_ID = "id";
    private final static String FLD_TITLE = "title";
    private final static String FLD_MESSAGE = "Message";
    private final static String FLD_DATE = "date";
    private final static String FLD_TIME = "time";
    private final static String FLD_FAVOURATE = "favourate";
    private final static String FLD_FLAG = "flag";



    // BRANCH  SINGLE table create statement---------------------------------------------------------------
    private static final String CREATE_TABLE_NOTIFICATION = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTIFICATION + "("
            + FLD_MESSAGE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FLD_TITLE + " TEXT,"
            + FLD_MESSAGE + " TEXT,"

           + FLD_FLAG + " INT"

            + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(CREATE_TABLE_NOTIFICATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);
        onCreate(db);
    }


    // Adding new volist list-------------------------------------------------------------------------------------------
    public void addMessage(Message messageList) throws SQLException {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(FLD_MESSAGE_ID, messageList.getId());
        values.put(FLD_TITLE, messageList.getTitle());
        values.put(FLD_MESSAGE, messageList.getMessage());

        db.insert(TABLE_NOTIFICATION, null, values);

        db.close(); // Closing database connection

        // Inserting Row
        // long postID = db.insert(TABLE_NOTIFICATION, null, values);
        // return postID;
    }

    public boolean messageExists(String id) {
        boolean found = false;
        String selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE id=" + id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        found = !(!cursor.moveToFirst() || cursor.getCount() == 0);

        return found;

    }

    public ArrayList<Message> getAllMessage() {
        ArrayList<Message> messageArrayList = new ArrayList<Message>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION + " ORDER BY id DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Message message = new Message();
                message.setId(cursor.getString(0));
                message.setTitle(cursor.getString(1));
                message.setMessage(cursor.getString(2));

                // Adding message to list
                messageArrayList.add(message);
            } while (cursor.moveToNext());
        }
        // return message list
        return messageArrayList;
    }

    // Deleting All Message
    public void deleteAllMessage() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTIFICATION, null, null);
        db.close();
    }

    // Deleting A Client
    public void deleteSingleMessage(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NOTIFICATION, FLD_MESSAGE_ID + " LIKE ? ", new String[]{"%" + id + "%"});
        db.close();
    }

    public void insertFavouriteMessage(String id){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        db.update(TABLE_NOTIFICATION,values,"id="+id,null);
    }
}
