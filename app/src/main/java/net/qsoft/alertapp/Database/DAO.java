package net.qsoft.alertapp.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.qsoft.alertapp.App.AppController;
import net.qsoft.alertapp.Model.Message;

import java.util.ArrayList;

import static net.qsoft.alertapp.Database.DBHelper.TABLE_ARCHIVE;
import static net.qsoft.alertapp.Database.DBHelper.TABLE_NOTIFICATION;

public class DAO {
	private static final String TAG = DAO.class.getSimpleName();

	// Database fields
	private SQLiteDatabase db;
	private DBHelper dbHelper;

	public DAO(Context context) {
		dbHelper = new DBHelper(context);
	}

	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		if (db != null && db.isOpen())
			db.close();
	}

	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Cursor getRecordsCursor(String sql, String[] param) {
		Cursor curs = null;
		curs = db.rawQuery(sql, param);
		return curs;
	}


	public void  execSQL(String sql, String[] param) throws SQLException {
		db.execSQL(sql, param);
	}

	public static void executeSQL(String sql, String[] param) {
		DAO da = new DAO(AppController.getInstance());
		da.open();
		try {
			da.execSQL(sql, param);
		}
		catch (Exception e){
			throw e;
		}
		finally {
			da.close();
		}
	}


	public ArrayList<Message> getAllArchiveMessage() {
		ArrayList<Message> messagesArrayList = new ArrayList<Message>();
		Message message = null;
		Cursor curs = null;

		try {
//			curs = db.query("SELECT " + TABLE_NOTIFICATION + " WHERE flag= "+Message.FLAG_FOR_FAVOURITE,new String[] { "[id]", "[title]",
//					"[message]","[status]", "[flag]"}, null, null, null, null, null);
//

			curs = db.query(TABLE_NOTIFICATION+ " WHERE flag=" + Message.FLAG_FOR_FAVOURITE, new String[] { "[id]", "[title]",
					"[message]","[status]", "[flag]"}, null, null, null, null, null);

			if (curs.moveToFirst()) {
				do {
					message = new Message(curs.getString(0), curs.getString(1), curs.getString(2),
							curs.getInt(3),curs.getInt(4));
					messagesArrayList.add(message);
				} while (curs.moveToNext());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e(TAG, e.toString());

		} finally {
			if (curs != null)
				curs.close();
		}

		return messagesArrayList;
	}

	public ArrayList<Message> getAllArchive() {
		ArrayList<Message> messagesArrayList = new ArrayList<>();
		Cursor curs = null;
		String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION + " WHERE flag= "+Message.FLAG_FOR_FAVOURITE ;

		 curs = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (curs.moveToFirst()) {
			do {
				Message message = new Message();
				message.setId(curs.getString(0));
				message.setTitle(curs.getString(1));
				message.setMessage(curs.getString(2));
				message.setStatus(curs.getInt(3));

				message.setFlag(curs.getInt(4));

				// Adding message to list
				messagesArrayList.add(message);
			} while (curs.moveToNext());
		}

		// return message list
		return messagesArrayList;

	}


	public ArrayList<Message> getAllMessage() {
		ArrayList<Message> messageArrayList = new ArrayList<Message>();
		Message message = null;
		Cursor curs = null;

		try {
			curs = db.query(TABLE_NOTIFICATION+ " ORDER BY id DESC", new String[] { "[id]", "[title]",
					"[message]", "[status]", "[flag]"}, null, null, null, null, null);

			if (curs.moveToFirst()) {
				do {
					message = new Message(curs.getString(0), curs.getString(1), curs.getString(2),
							curs.getInt(3),1);
					messageArrayList.add(message);
				} while (curs.moveToNext());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e(TAG, e.toString());

		} finally {
			if (curs != null)
				curs.close();
		}

		return messageArrayList;
	}


	public ArrayList<Message> gettingAllDraftMessage() {
		ArrayList<Message> messageArrayList = new ArrayList<Message>();
		Message message = null;
		Cursor curs = null;

		try {
			curs = db.query(TABLE_ARCHIVE+ " ORDER BY id DESC", new String[] { "[id]", "[title]",
					"[message]", "[time]"}, null, null, null, null, null);

			if (curs.moveToFirst()) {
				do {
					message = new Message(curs.getString(0), curs.getString(1),
							curs.getString(2), curs.getString(3));
					messageArrayList.add(message);
				} while (curs.moveToNext());
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e(TAG, e.toString());

		} finally {
			if (curs != null)
				curs.close();
		}

		return messageArrayList;
	}

	public boolean messageExists(String id) {
		boolean found = false;
		Cursor curs = null;
		String selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE id=" + id;
		 curs = db.rawQuery(selectQuery, null);

		found = !(!curs.moveToFirst() || curs.getCount() == 0);

		return found;

	}

	// Deleting All Message
	public void deleteAllMessage() {
		db.delete(TABLE_NOTIFICATION, null, null);
		db.close();
	}

	// Deleting A Client
	public void deleteSingleMessage(String id) {
		db.delete(TABLE_NOTIFICATION, "[id]=?",
				new String[]{id});
		//db.delete(TABLE_NOTIFICATION, id + " LIKE ? ", new String[]{"%" + id + "%"});
		db.close();
	}

	public void UpdateStatus(int status, String id) {
		ContentValues cv = new ContentValues();

		cv.clear();
		cv.put("[status]", status);

		db.update(TABLE_NOTIFICATION, cv, "[id]=?",
				new String[]{id});

		//db.update(TABLE_NOTIFICATION, cv, null, null);
	}

	public void UpdateFlag(int flag, String id) {
		ContentValues cv = new ContentValues();

		cv.clear();
		cv.put("[flag]", flag);

		db.update(TABLE_NOTIFICATION, cv, "[id]=?",
				new String[]{id});

		//db.update(TABLE_NOTIFICATION, cv, null, null);
	}
}
