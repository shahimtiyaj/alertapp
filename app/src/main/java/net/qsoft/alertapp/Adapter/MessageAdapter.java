package net.qsoft.alertapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qsoft.alertapp.Activity.ArchiveMessage;
import net.qsoft.alertapp.Activity.DetailsActivity;
import net.qsoft.alertapp.App.AppController;
import net.qsoft.alertapp.Database.DAO;
import net.qsoft.alertapp.Database.DatabaseHelper;
import net.qsoft.alertapp.Model.Message;
import net.qsoft.alertapp.R;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import static net.qsoft.alertapp.Database.DBHelper.TABLE_ARCHIVE;
import static net.qsoft.alertapp.Database.DBHelper.TABLE_NOTIFICATION;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    private Context context;
    private ArrayList<Message> messageArrayList;
    private ArrayList<Message> messageArrayListFiltered;
    private messageAdapterListener listener;
    public Button btnArchive;

    private static final int MESSAGE_ITEM_VIEW_TYPE = 0;
    private static final int AD_VIEW_TYPE = 1;
    public static String local_id;
    public int f = 0;

    //nex3z notification  badge --
    public com.nex3z.notificationbadge.NotificationBadge mBadge;
    public int mCount = 1;

    TextView smsCountTxt;
    int pendingSMSCount = 10;

    public MessageAdapter(Context context, ArrayList<Message> messageArrayList, messageAdapterListener listener) {
        this.context = context;
        this.messageArrayList = messageArrayList;
        this.listener = listener;
        this.messageArrayListFiltered = messageArrayList;

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case MESSAGE_ITEM_VIEW_TYPE:
            default:

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_list_item, parent, false);

               /* Animation animationY = new TranslateAnimation(
                        view.getWidth() / 1, 0, view.getHeight() / 2,
                        0);
                animationY.setDuration(1000);
                view.startAnimation(animationY);
                animationY = null; */

                return new MessageViewHolder(view);

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case MESSAGE_ITEM_VIEW_TYPE:
            default:
                MessageViewHolder messageViewHolder = (MessageViewHolder) holder;
                setMessageView(messageViewHolder, position);
                break;

        }
    }

    private void setMessageView(final MessageViewHolder holder, final int position) {
        final Message messages = (Message) messageArrayListFiltered.get(position);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        int status = messages.getStatus();
        final String fv_id = messages.getId();
        final String fv_title = messages.getTitle();
        final String fv_message = messages.getMessage();


        //  DatabaseHelper databaseHelper = new DatabaseHelper(context);


        // holder.id.setText(messages.getId());
        holder.title.setText(messages.getTitle());
        // holder.message.setText(messages.getMessage());
        holder.message.setText(messages.getMessage().substring(0, 5));


        //    mBadge.setNumber(mCount);


        if (status == 0)

            holder.title.setTypeface(holder.title.getTypeface(), android.graphics.Typeface.BOLD);

        // btnDelete.setVisibility(View.INVISIBLE);

        btnArchive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DAO dao = new DAO(AppController.getInstance());
                dao.open();
                dao.UpdateFlag(1, fv_id);

                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());


                DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_ARCHIVE + "(id, title, message, time) " +
                                "VALUES(?, ?, ?, ?)",

                        new String[]{fv_id, fv_title, fv_message, currentDateTimeString});


                Toast.makeText(context, "Archived!: " + fv_title, Toast.LENGTH_LONG).show();
            }
        });

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.id.setTextColor(Color.BLUE);

                //  mCount = 0;
                // mBadge.setNumber(mCount);

                Intent intent = new Intent(context, DetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id", messageArrayList.get(position).getId());
                intent.putExtra("title", messageArrayList.get(position).getTitle());
                intent.putExtra("message", messageArrayList.get(position).getMessage());
                intent.putExtra("status", messageArrayList.get(position).getStatus());
                context.startActivity(intent);

                //  Toast.makeText(context, "Clicked!", Toast.LENGTH_LONG).show();


            }
        });

    }

    @Override
    public int getItemViewType(int position) {

        return MESSAGE_ITEM_VIEW_TYPE;

    }


    @Override
    public int getItemCount() {
        return messageArrayListFiltered.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout layoutRightButtons;
        private TextView id, message, title, date, time;

        CardView card_view;

        public MessageViewHolder(View itemView) {
            super(itemView);
            layoutRightButtons = itemView.findViewById(R.id.layoutRightButtons);
            card_view = (CardView) itemView.findViewById(R.id.card_view);

            id = itemView.findViewById(R.id.id);
            title = itemView.findViewById(R.id.title);
            message = itemView.findViewById(R.id.message);
            btnArchive = itemView.findViewById(R.id.btnArchive);
            //  mBadge = itemView.findViewById(R.id.badge);


            //Long Press
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //DatabaseHelper databaseHelper = new DatabaseHelper(context);
                    //local_id = messages.getId();
                    //databaseHelper.deleteSingleMessage(local_id);
                    delete(getAdapterPosition());
                    Toast.makeText(context, "Deleted!", Toast.LENGTH_LONG).show();
                    return false;
                }
            });


//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    // send selected contact in callback
//                    listener.onMessageSelected(messageArrayListFiltered.get(getOldPosition()));
//                }
//            });


        }


    }


    public void delete(int position) {
        final Message messages = (Message) messageArrayListFiltered.get(position);
        // DatabaseHelper databaseHelper = new DatabaseHelper(context);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        local_id = messages.getId();
        dao.deleteSingleMessage(local_id);
        // messageArrayListFiltered.remove(position);
        // notifyItemRemoved(position);

        dao.close();
    }

    private void setupBadge() {

        if (smsCountTxt != null) {
            if (pendingSMSCount == 0) {
                if (smsCountTxt.getVisibility() != View.GONE) {
                    smsCountTxt.setVisibility(View.GONE);
                }
            } else {
                smsCountTxt.setText(String.valueOf(Math.min(pendingSMSCount, 99)));
                if (smsCountTxt.getVisibility() != View.VISIBLE) {
                    smsCountTxt.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    messageArrayListFiltered = messageArrayList;
                } else {
                    ArrayList<Message> filteredList = new ArrayList<>();
                    for (Message row : messageArrayList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getMessage().contains(charSequence)) {
                            filteredList.add(row);
                        }

                    }

                    messageArrayListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = messageArrayListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                messageArrayListFiltered = (ArrayList<Message>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface messageAdapterListener {
        void onMessageSelected(Message message);
    }

}
