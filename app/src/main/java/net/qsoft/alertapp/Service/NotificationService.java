package net.qsoft.alertapp.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import net.qsoft.alertapp.Activity.MessageActivity;
import net.qsoft.alertapp.Adapter.MessageAdapter;
import net.qsoft.alertapp.App.AppController;
import net.qsoft.alertapp.Database.DAO;
import net.qsoft.alertapp.Database.DatabaseHelper;
import net.qsoft.alertapp.Model.Message;
import net.qsoft.alertapp.NetworkService.Utils;
import net.qsoft.alertapp.NetworkService.VolleyCustomRequest;
import net.qsoft.alertapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.android.volley.VolleyLog.d;
import static com.android.volley.VolleyLog.v;
import static net.qsoft.alertapp.Database.DBHelper.TABLE_NOTIFICATION;

public class NotificationService extends Service {
    public static int flag = 0;
    private static final String TAG = NotificationService.class.getSimpleName();
    DatabaseHelper databaseHelper;
    private MessageAdapter adapter;
    public static int sum = 1;
    int id;
    public ArrayList<Message> messagesList;
    MessageAdapter.messageAdapterListener listener;

    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    public NotificationService() {

    }

    @Override
    public int onStartCommand(Intent intent, final int flags, int startId) {

        if (Utils.isNetworkAvailable(this)) {

            getNotifications();

        }

        return Service.START_NOT_STICKY;
    }


    public void getNotifications() {
        databaseHelper = new DatabaseHelper(getApplicationContext());
        messagesList = new ArrayList<Message>();
        adapter = new MessageAdapter(getApplicationContext(), messagesList, listener);

        String url = "https://bytelab.000webhostapp.com/Product/notification.php?";

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, "Server Response: " + response.toString());

                        try {
                            v("Response:%n %s", response.toString(4));
                            for (int i = 0; i < response.length(); i++) {

                                JSONObject notity = (JSONObject) response
                                        .get(i);

                                Message message = new Message();
                                id = Integer.parseInt(message.setId(notity.getString("id")));
                                String title = message.setTitle(notity.getString("title"));
                                String mbody = message.setMessage(notity.getString("message"));
                                messagesList.add(message);

                                // String id = notity.getString("id");
                                // String mbody = notity.getString("message");
                                int status = Integer.parseInt(notity.getString("status"));
                                DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_NOTIFICATION + "(id, title, message) " +
                                                "VALUES(?, ?, ?)",

                                        new String[]{String.valueOf(id),
                                                title, mbody});

                                sharedpreferences = getApplicationContext().getSharedPreferences(
                                        MyPREFERENCES, 0);
                                flag = sharedpreferences.getInt("f", flag);

                                if (flag != notity.getInt("id")) {
                                    notifyUser(title, mbody, sum);
                                    flag = notity.getInt("id");
                                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedpreferences.edit();
                                    editor.putInt("f", flag);
                                    editor.commit();
                                    sum = sum + 1;
                                }
                            }

                            adapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                      //  saveMessage(messagesList);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                d(TAG, "Error: " + volleyError.getMessage());

                if (volleyError instanceof NetworkError) {
                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                } else if (volleyError instanceof ServerError) {
                    Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();

                } else if (volleyError instanceof AuthFailureError) {
                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();


                } else if (volleyError instanceof ParseError) {
                    Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();

                } else if (volleyError instanceof NoConnectionError) {
                    Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();

                } else if (volleyError instanceof TimeoutError) {
                    Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();

                }
            }
        });

        Volley.newRequestQueue(this).add(req);
    }

    public void UpdateStatus() {

        String hitURL = "https://bytelab.000webhostapp.com/Product/update.php?";

        HashMap<String, String> params = new HashMap<>();
        params.put("accept", "accept");

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d(TAG, "Update Server Response: " + response.toString());
                        try {
                            Log.v("Response:%n %s", response.toString(4));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.d(TAG, "Error: " + volleyError.getMessage());

                        if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();
                        } else if (volleyError instanceof ServerError) {
                            Toast.makeText(getApplicationContext(), "The server could not be found. Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof AuthFailureError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();


                        } else if (volleyError instanceof ParseError) {
                            Toast.makeText(getApplicationContext(), "Parsing error! Please try again after some time!!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Cannot connect to Internet...Please check your connection!", Toast.LENGTH_SHORT).show();

                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection TimeOut! Please check your internet connection", Toast.LENGTH_SHORT).show();

                        }
                    }
                });

        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(postRequest);
    }

//    private void saveMessage(ArrayList<Message> messagesList) {
//
//        for (int i = 0; i < messagesList.size(); i++) {
//            boolean found = databaseHelper.messageExists(messagesList.get(i).getId());
//            if (!found) {
//                databaseHelper.addMessage(new Message(messagesList.get(i).getId(),
//                        messagesList.get(i).getTitle(), messagesList.get(i).getMessage(),
//                         messagesList.get(i).getStatus()));
//            }
//        }
//    }

    public String getNotifyTime() {
        DateFormat timeFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        return timeFormat.format(new Date());
    }

    public String getNotifyDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy", Locale.getDefault());
        return dateFormat.format(new Date());
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void notifyUser(String title, String message, int sum) {
        int mId = 001;
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notify)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_notify))
                        .setContentTitle("New Message from: " + title)
                        .setContentText(message)
                        .setOnlyAlertOnce(true)
                        .setOngoing(true)
                        .setNumber(sum)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setAutoCancel(true);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MessageActivity.class);
        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MessageActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(mId, mBuilder.build());
    }


    private void LocalNotify() {
        messagesList = new ArrayList<Message>();
        databaseHelper = new DatabaseHelper(getApplicationContext());

        try {

            ArrayList<Message> messages = databaseHelper.getAllMessage();

            for (Message message : messages) {
                String id1 = message.getId();
                String title = message.getTitle();
                String sms = message.getMessage();
                if (flag != id) {
                    notifyUser(title, sms, sum);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
